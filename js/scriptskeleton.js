//Our common starting point from where we branched. We worked on the main initializations for startup and the validation function
// See end for credits

window.addEventListener("DOMContentLoaded", main, false); // For checking how many fields have correct input

function main(){
	let templateProjArray = [];
	let checklist = [false, false, false, false, false, false, false, false];
	
	let addButton = document.getElementById("addButton");
	addButton.setAttribute("disabled", "");
	
	// Getting all the fields into variables.
	let projectId = document.getElementById("projectId");
	let ownerName = document.getElementById("ownerName");
	let title = document.getElementById("title");
	let category = document.getElementById("category");
	let hours = document.getElementById("hours");
	let rate = document.getElementById("rate");
	let stats = document.getElementById("status");
	let shortDesc = document.getElementById("shortDesc");
	shortDesc.textContent = ""; // The textarea by default seems to have a few lines of whitespace, so this removes it.
	
	let checkBox = document.getElementsByClassName("checkBox"); // Where all the checkmark or X symbols will go, next to each input.
	let errorMsg = document.getElementsByClassName("errorMsg"); // For the field feedback.
	scratch();
	
	// Sets all fields to have an X and the respective error message when they're empty. Happens on page load and when the reset button is pressed, or right after a project is successfully added.
	function scratch(){
		for (let i = 0; i < checkBox.length; i++){
			checkBox[i].textContent ="❌";
		}
		
		for (let i = 0; i < checkBox.length; i++){
			errorMsg[i].textContent ="Field can't be empty.";
		}
	}
	
	let statusBar = document.getElementById("statusBar");
	statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project."; // The default status bar message.
	
	let valSet = document.getElementsByClassName("validate"); // Puts all the form fields into this variable.
	Array.from(valSet).forEach(function(item){ // forEach loop
		// Select type inputs get the change event listener.
		if (item.tagName === "SELECT"){
			item.addEventListener("change", validateForm, false);
		}
		
		// Text and textarea get an event for keyup.
		else{
			item.addEventListener("keyup", validateForm, false);
		}
	});
	
	function validateForm(e){ // All form validation occurs here.
		let complaintMessage = e.target.parentElement.lastElementChild;
		let statusSymbol = e.target.nextElementSibling;
		let statusSymbolForTextArea = e.target.previousElementSibling;
		
		if (e.key === "Enter"){
			e.preventDefault(); // This prevents submitting and refreshing the page.
			return;
		}
		
		const alNum = /^(\d|\w|\s)+$/;
		const alph = /^[A-Z, a-z]+$/;
		const numb = /^[0-9]+$/;
		
		if (e.target.value != ""){
			// Select items can't have an "incorrect" non-empty value. So any choice for it adds the checkmark.
			if (e.target.tagName === "SELECT"){
				statusSymbol.textContent = "\u2705";
				complaintMessage.textContent = "";
			}
			
			else{
				if (e.target.value.charAt(0) == " " || e.target.value.charAt(0) == "\n"){
					if (e.target.id === "shortDesc"){
						statusSymbolForTextArea.textContent = "❌";
					}
					
					else{
						statusSymbol.textContent = "❌";
					}
					
					complaintMessage.textContent = "No spaces at the start"; // Design choice, no spaces are allowed at the start to keep things consistent and avoid extreme cases.
				}
				
				else{
					// Basically, focusing on the keyup at each text/textarea input and if the respective regex test returns true...
					if ((e.target.id === "ownerName" && alph.test(e.target.value)) || ((e.target.id === "hours" || e.target.id === "rate") && numb.test(e.target.value)) || ((e.target.id === "projectId" || e.target.id === "title" || e.target.id === "shortDesc") && alNum.test(e.target.value))){
						if (e.target.id === "shortDesc"){
							statusSymbolForTextArea.textContent = "\u2705"; // In the html, the checkmark is placed before the textarea...
						}
						
						else{
							statusSymbol.textContent = "\u2705"; // ... meanwhile it's after all text inputs.
						}
						
						complaintMessage.textContent = ""; // Whenever this occurs, it's to remove the form field feedback ("can't be empty", "no numbers", etc).
						
						// This ensures Project IDs are unique. It compares them all uppercase and spaces removed. Again, it's a design choice to ensure meaningful project IDs (no project5, PrOjeCT5, Project 5, etc).
						// The particular if block here is checking against the projects table...
						if (e.target.id === "projectId"){
							if (templateProjArray.length > 0){
								
								for (let i = 0; i < templateProjArray.length; i++){
									if (e.target.value.replace(/ /g, "").toUpperCase() === templateProjArray[i].projectId.replace(/ /g, "").toUpperCase()){
										statusSymbol.textContent = "❌";
										complaintMessage.textContent = "Project ID must be unique (already exists in table).";
										break;
									}
								}
							}
							
							// While this one is checking against the one in localStore.
							if (localStorage.length > 0){
								let localRecord = JSON.parse(localStorage.getItem("projectSet"));
								
								for (let i = 0; i < localRecord.length; i++){
									if (e.target.value.replace(/ /g, "").toUpperCase() === localRecord[i].projectId.replace(/ /g, "").toUpperCase()){
										statusSymbol.textContent = "❌";
										complaintMessage.textContent = "Project ID must be unique (already exists in local storage).";
										break;
									}
								}
							}
						}
					}
					
					// If the user input is incorrect from the above requirements, the X is placed accordingly, as well as the contextual feedback message for what's wrong in the input.
					else{
						if (e.target.id === "shortDesc"){
							statusSymbolForTextArea.textContent = "❌";
						}
						
						else{
							statusSymbol.textContent = "❌";
						}
						
						if (e.target.id === "ownerName"){
							complaintMessage.textContent = "Only letters";
						}
						
						else if (e.target.id === "hours" || e.target.id === "rate"){
							complaintMessage.textContent = "Only whole numbers 0 or more";
						}
						
						else{
							complaintMessage.textContent = "Only letters and/or numbers";
						}
					}
				}
			}
		}
		
		// Feedback for if a field is empty. Ensures the user knows what they're missing before being able to add a project.
		else{
			if (e.target.id === "shortDesc"){
				statusSymbolForTextArea.textContent = "❌";
			}
			
			else{
				statusSymbol.textContent = "❌";
			}
			
			complaintMessage.textContent = "Field can't be empty";
		}
		
		// If there's a checkmark for a field, the respective boolean in the array is set to true. If not, it's set to false.
		for (let i = 0; i < valSet.length; i++){
			if (valSet[i].nextElementSibling.textContent === "\u2705" || valSet[i].previousElementSibling.textContent === "\u2705"){
				checklist[i] = true;
			}
			
			else{
				checklist[i] = false;
			}
		}
		
		// This checks how many true boolean values are in the array.
		let counter = 0;
		
		for (let i = 0; i < checklist.length; i++){
			if (checklist[i]){
				counter++;
			}
		}
		
		// 8 is needed for full form validation in order to enable the Add button to be clicked...
		if (counter === 8){
			addButton.removeAttribute("disabled");
			statusBar.firstElementChild.textContent = "Form validated. You may now add the project to the table.";
		}
		
		// ... otherwise, it remains disabled.
		else{
			addButton.setAttribute("disabled", "");
			statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";
		}
	}
	
	let resetInputs = function(){

	}
	
	let resetButton = document.getElementById("resetButton");
	resetButton.addEventListener("click", resetInputs, false);
	
	function addProject(){
		
	}
	
	function buildTable(){ 
		
	}
	
	function updateField(){
		
	}
	
	function removeRow(){
		
	}
	
	let writeLocal = document.getElementById("writeLocal");
	writeLocal.addEventListener("click", saveLocal, false);
	
	function saveLocal(){
		
	}
	
	let appendLocal = document.getElementById("appendLocal");
	appendLocal.addEventListener("click", addLocal, false);
	
	function addLocal(){
		
	}
	
	let clearLocal = document.getElementById("clearLocal");
	clearLocal.addEventListener("click", eraseLocal, false);
	
	function eraseLocal(){
		
	}
	
	let readLocal = document.getElementById("readLocal");
	readLocal.addEventListener("click", loadLocal, false);
	
	function loadLocal(){
		
	}
	
	let querySearch = document.getElementById("querySearch");
	querySearch.addEventListener("keyup", filterSearch, false);
	
	function filterSearch(){
		
	}
	
}

/*
  CREDITS:
  
  -toUpperCase (and toLowerCase) from Java I
  
  -Removing spaces to compare values: https://www.geeksforgeeks.org/how-to-remove-spaces-from-a-string-using-javascript/
  
  -setTimeout (used in previous labs): https://developer.mozilla.org/en-US/docs/Web/API/setTimeout
*/