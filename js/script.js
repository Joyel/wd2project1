// See end for credits

window.addEventListener("DOMContentLoaded", function(){
	let templateProjArray = [];
	let checklist = [false, false, false, false, false, false, false, false]; // For checking how many fields have correct input
	
	let addButton = document.getElementById("addButton");
	addButton.setAttribute("disabled", "");
	
	// Getting all the fields into variables.
	let projectId = document.getElementById("projectId");
	let ownerName = document.getElementById("ownerName");
	let title = document.getElementById("title");
	let category = document.getElementById("category");
	let hours = document.getElementById("hours");
	let rate = document.getElementById("rate");
	let stats = document.getElementById("status");
	let shortDesc = document.getElementById("shortDesc");
	shortDesc.textContent = ""; // The textarea by default seems to have a few lines of whitespace, so this removes it.
	
	let checkBox = document.getElementsByClassName("checkBox"); // Where all the checkmark or X symbols will go, next to each input.
	let errorMsg = document.getElementsByClassName("errorMsg"); // For the field feedback.
	scratch();
	
	// Sets all fields to have an X and the respective error message when they're empty. Happens on page load and when the reset button is pressed, or right after a project is successfully added.
	function scratch(){
		for (let i = 0; i < checkBox.length; i++){
			checkBox[i].textContent ="❌";
		}
		
		for (let i = 0; i < checkBox.length; i++){
			errorMsg[i].textContent ="Field can't be empty.";
		}
	}
	
	let statusBar = document.getElementById("statusBar");
	statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project."; // The default status bar message.
	
	let valSet = document.getElementsByClassName("validate"); // Puts all the form fields into this variable.
	Array.from(valSet).forEach(function(item){ // forEach loop
		// Select type inputs get the change event listener.
		if (item.tagName === "SELECT"){
			item.addEventListener("change", validateForm, false);
		}
		
		// Text and textarea get an event for keyup.
		else{
			item.addEventListener("keyup", validateForm, false);
		}
	});
	
	function validateForm(e){ // All form validation occurs here.
		let complaintMessage = e.target.parentElement.lastElementChild;
		let statusSymbol = e.target.nextElementSibling;
		let statusSymbolForTextArea = e.target.previousElementSibling;
		
		if (e.key === "Enter"){
			e.preventDefault(); // This prevents submitting and refreshing the page.
			return;
		}
		
		const alNum = /^(\d|\w|\s)+$/;
		const alph = /^[A-Z, a-z]+$/;
		const numb = /^[0-9]+$/;
		
		if (e.target.value != ""){
			// Select items can't have an "incorrect" non-empty value. So any choice for it adds the checkmark.
			if (e.target.tagName === "SELECT"){
				statusSymbol.textContent = "\u2705";
				complaintMessage.textContent = "";
			}
			
			else{
				if (e.target.value.charAt(0) == " " || e.target.value.charAt(0) == "\n"){
					if (e.target.id === "shortDesc"){
						statusSymbolForTextArea.textContent = "❌";
					}
					
					else{
						statusSymbol.textContent = "❌";
					}
					
					complaintMessage.textContent = "No spaces at the start"; // Design choice, no spaces are allowed at the start to keep things consistent and avoid extreme cases.
				}
				
				else{
					// Basically, focusing on the keyup at each text/textarea input and if the respective regex test returns true...
					if ((e.target.id === "ownerName" && alph.test(e.target.value)) || ((e.target.id === "hours" || e.target.id === "rate") && numb.test(e.target.value)) || ((e.target.id === "projectId" || e.target.id === "title" || e.target.id === "shortDesc") && alNum.test(e.target.value))){
						if (e.target.id === "shortDesc"){
							statusSymbolForTextArea.textContent = "\u2705"; // In the html, the checkmark is placed before the textarea...
						}
						
						else{
							statusSymbol.textContent = "\u2705"; // ... meanwhile it's after all text inputs.
						}
						
						complaintMessage.textContent = ""; // Whenever this occurs, it's to remove the form field feedback ("can't be empty", "no numbers", etc).
						
						// This ensures Project IDs are unique. It compares them all uppercase and spaces removed. Again, it's a design choice to ensure meaningful project IDs (no project5, PrOjeCT5, Project 5, etc).
						// The particular if block here is checking against the projects table...
						if (e.target.id === "projectId"){
							if (templateProjArray.length > 0){
								
								for (let i = 0; i < templateProjArray.length; i++){
									if (e.target.value.replace(/ /g, "").toUpperCase() === templateProjArray[i].projectId.replace(/ /g, "").toUpperCase()){
										statusSymbol.textContent = "❌";
										complaintMessage.textContent = "Project ID must be unique (already exists in table).";
										break;
									}
								}
							}
							
							// While this one is checking against the one in localStore.
							if (localStorage.length > 0){
								let localRecord = JSON.parse(localStorage.getItem("projectSet"));
								
								for (let i = 0; i < localRecord.length; i++){
									if (e.target.value.replace(/ /g, "").toUpperCase() === localRecord[i].projectId.replace(/ /g, "").toUpperCase()){
										statusSymbol.textContent = "❌";
										complaintMessage.textContent = "Project ID must be unique (already exists in local storage).";
										break;
									}
								}
							}
						}
					}
					
					// If the user input is incorrect from the above requirements, the X is placed accordingly, as well as the contextual feedback message for what's wrong in the input.
					else{
						if (e.target.id === "shortDesc"){
							statusSymbolForTextArea.textContent = "❌";
						}
						
						else{
							statusSymbol.textContent = "❌";
						}
						
						if (e.target.id === "ownerName"){
							complaintMessage.textContent = "Only letters";
						}
						
						else if (e.target.id === "hours" || e.target.id === "rate"){
							complaintMessage.textContent = "Only whole numbers 0 or more";
						}
						
						else{
							complaintMessage.textContent = "Only letters and/or numbers";
						}
					}
				}
			}
		}
		
		// Feedback for if a field is empty. Ensures the user knows what they're missing before being able to add a project.
		else{
			if (e.target.id === "shortDesc"){
				statusSymbolForTextArea.textContent = "❌";
			}
			
			else{
				statusSymbol.textContent = "❌";
			}
			
			complaintMessage.textContent = "Field can't be empty";
		}
		
		// If there's a checkmark for a field, the respective boolean in the array is set to true. If not, it's set to false.
		for (let i = 0; i < valSet.length; i++){
			if (valSet[i].nextElementSibling.textContent === "\u2705" || valSet[i].previousElementSibling.textContent === "\u2705"){
				checklist[i] = true;
			}
			
			else{
				checklist[i] = false;
			}
		}
		
		// This checks how many true boolean values are in the array.
		let counter = 0;
		
		for (let i = 0; i < checklist.length; i++){
			if (checklist[i]){
				counter++;
			}
		}
		
		// 8 is needed for full form validation in order to enable the Add button to be clicked...
		if (counter === 8){
			addButton.removeAttribute("disabled");
			statusBar.firstElementChild.textContent = "Form validated. You may now add the project to the table.";
		}
		
		// ... otherwise, it remains disabled.
		else{
			addButton.setAttribute("disabled", "");
			statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";
		}
	}

	let rowAdd = document.getElementById("addRowsHere");

	function buildTable(buildThis){ 
		let sortedArr = sort(buildThis); //sends the project array to sort function, which processes it in order of project id and returns
		
		rowAdd.textContent = ""; //table is redrawn from scratch each time
		
		for (let i = 0; i < sortedArr.length; i++){
			let tableRow = document.createElement("tr");
			
			for (let j = 0; j < Object.values(sortedArr[i]).length; j++){
				let cell = document.createElement("td");
				let cellContent = document.createTextNode(Object.values(sortedArr[i])[j]);
				cell.appendChild(cellContent);
				tableRow.appendChild(cell);
			}
			
			let cell9 = document.createElement("td"); //the edit icon of each row
			let editCell = document.createElement("img");
			editCell.setAttribute("src", "media/edit.png");
			editCell.setAttribute("alt", "edit project");
			editCell.setAttribute("class", "update");
			cell9.appendChild(editCell);
			tableRow.appendChild(cell9);
			
			let cell10 = document.createElement("td"); //the trash icon of each row
			let trashCell = document.createElement("img");
			trashCell.setAttribute("src", "media/delete.png");
			trashCell.setAttribute("alt", "delete project");
			trashCell.setAttribute("class", "remove");
			cell10.appendChild(trashCell);
			tableRow.appendChild(cell10);
			
			rowAdd.appendChild(tableRow);
		}
		
		let changeFields = document.getElementsByClassName("update");
		for (let i = 0; i < changeFields.length; i++){
			changeFields[i].addEventListener("click", updateField, false);
		}
		
		let takeOut = document.getElementsByClassName("remove");
		for (let i = 0; i < takeOut.length; i++){
			takeOut[i].addEventListener("click", function(e){
				if (templateProjArray.length > 0){
					let theProjectId = "";
					let location = 0;
					let trashCans = document.getElementsByClassName("remove");
					
					//storing the row number to be deleted, gets the projectid of it to remove that object from the array
					for (let i = 0; i < trashCans.length; i++){ 
						if (trashCans[i] === e.target){
							theProjectId = templateProjArray[i].projectId;
							location = i;
							break;
						}
					}
					
					//removing the object at the specified location in the array, leaving a null value
					if (confirm("Are you sure you want to delete " + theProjectId + "?")){
						templateProjArray.splice(location, 1, 0); 
						
						//moving all the objects before the null value up by 1 in position
						for (let i = location; i > 0; i--){
							templateProjArray[i] = templateProjArray[i - 1]; 
						}
						
						templateProjArray.shift(); //removes the first array value, which is where the null value is
						buildTable(templateProjArray);
						let localRecord = JSON.parse(localStorage.getItem("projectSet"));

						//removing the project with the stored projectid from the local storage
						if (localRecord != null && localStorage.length > 0){ 
							let found = -1;
							for (let i = 0; i < localRecord.length; i++){
								if (localRecord[i].projectId == theProjectId){
									found = i;
									break;
								}
							}
							if (found > -1 && localRecord.length > 1){
								localRecord.splice(found, 1, 0);
								for (let i = found; i > 0; i--){
									localRecord[i] = localRecord[i - 1];
								}
								localRecord.shift();
								localStorage.setItem("projectSet", JSON.stringify(localRecord));
							}
							else if (found > -1){
								localStorage.clear();
							}
						}
						//updates status bar and resets to default after a short timer
						statusBar.firstElementChild.textContent = "Deleted " + theProjectId + ". There is/are now " + (localStorage.length == 0 ? 0 : (localStorage.length + "other ")) + " project(s) in localStorage.";
						setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
					}
				}
			}, false);
		}
	}

	// When clicking the edit icon beside a row.
	function updateField(e){
		e.target.setAttribute("src", "media/save.png");
		let rowIndex = 0;
		
		let notePads = document.getElementsByClassName("update");
		
		for (let i = 0; i < notePads.length; i++){ // This captures which row exactly the edit icon was clicked.
			if (notePads[i] === e.target){
				rowIndex = i;
				break;
			}
		}
		
		let tableRows = rowAdd.getElementsByTagName("tr");
		let targetRow = tableRows[rowIndex];
		
		for (let i = 0; i < 8; i++){ // When each cell turns into a text input field, its default values are the current ones before editing.
			let inputBox = document.createElement("input");
			inputBox.value = Object.values(templateProjArray[rowIndex])[i];
			inputBox.setAttribute("type", "text");
			inputBox.style.width = "45%";
			targetRow.children[i].textContent = "";
			targetRow.children[i].appendChild(inputBox);
		}
		
		e.target.removeEventListener("click", updateField, false); // Prevent conflict whe clicking the save icon that was the edit icon before.
		e.target.addEventListener("click", confirmChanges, false);
		
		function confirmChanges(e){
			let counter = 0;
			
			for (let x in templateProjArray[rowIndex]){ // This edits all the specified object's property values to what's in the text inputs.
				templateProjArray[rowIndex][x] = targetRow.children[counter].firstElementChild.value;
				counter++;
			}
			
			buildTable(templateProjArray); // Rebuild the table with that particular object updated.
			
			// Brings back the edit icon, replaces its event listener back to the one for editing.
			e.target.setAttribute("src", "media/edit.png");
			e.target.removeEventListener("click", confirmChanges, false);
			e.target.addEventListener("click", updateField, false);
			
			statusBar.firstElementChild.textContent = "Project at row " + (rowIndex + 1)+ " updated.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
		}
	}

	let appendLocal = document.getElementById("appendLocal");
	appendLocal.addEventListener("click", addLocal, false);
	
	//function to append to local storage
	function addLocal(){
		if (templateProjArray.length > 0){
			if (localStorage.length > 0){
				let localRecord = JSON.parse(localStorage.getItem("projectSet"));
				
				combineArrays(templateProjArray, localRecord); //if local storage isn't empty, actual appending occurs
			}
			
			else{
				saveLocal(); //otherwise it's the same as save/overwrite (overwriting empty is the same as appending empty)
			}
		}
		
		else{
			statusBar.firstElementChild.textContent = "Current project array empty. Nothing to append/update.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
		}
	}

	// Efficient 2-in-1 that works for both loading from localStorage and appending to localStorage. Which of the 2 will occur is differentiated by which one's the donor and which is the recipient in the passed inputs.
	function combineArrays(donorArray, recipientArray){
		if (recipientArray.length > 0){ // For the case where the array that will be built from combining both arrays isn't empty.
			for (let i = 0; i < donorArray.length; i++){
				let matchFound = false;
				
				for (let j = 0; j < recipientArray.length; j++){ // Checks each project object in the donor if it's already in the recipient or not (based on project ID)...
					if (donorArray[i].projectId === recipientArray[j].projectId){
						recipientArray[j] = donorArray[i]; // If it's in the array already, it overwrites that one, which is pretty much just updating its values.
						matchFound = true;
						break;
					}
				}
				
				if (!matchFound){ // If it's not in the array, it just simply adds it.
					recipientArray.push(donorArray[i]);
				}
			}
		}
		
		else{
			recipientArray = donorArray; // If the array that's being built from combining both is itself empty, just make it the donor array.
		}
		
		if (donorArray === templateProjArray){ // The differentiating step. If the donor is the current project array, it's providing its project objects to add/update in the localStorage (append local)...
			localStorage.setItem("projectSet", JSON.stringify(recipientArray));
			statusBar.firstElementChild.textContent = "Table projects added to localStorage (combined/updated). There is/are now " + JSON.parse(localStorage.getItem("projectSet")).length + " project(s) in localStorage.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 5000);
		}
		
		else{
			templateProjArray = recipientArray; // ... otherwise, it's the other way around. The array in localStorage is providing its project objects to load to the current array (load local).
			statusBar.firstElementChild.textContent = "Projects from localStorage loaded into current project array (combined/updated). There is/are now " + templateProjArray.length + " project(s) in the table and current project array.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 5000);
		}
	}

	// This is to sort the array according to projectId.
	function sort(toSort){
		if (toSort.length > 1){
			for (let i = 0; i < toSort.length - 1; i++){ // Compare the current index...
				for (let j = i + 1; j < toSort.length; j++){ // ... to the one after it.
					let limit = toSort[i].projectId.toString().length;
					
					if (limit > toSort[j].projectId.toString().length){ // Basically if the current index String length is longer than the next index String...
						limit = toSort[j].projectId.toString().length; // ... it will only check along the length of the shorter String.
					}
					
					let same = true;
					for (let k = 0; k < limit; k++){ // Checks if the 2 compared Strings are identical along the length of the shorter one...
						if (toSort[i].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k) != toSort[j].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k)){
							same = false;
							break;
						}
					}
					
					for (let k = 0; k < limit; k++){
						if (toSort[i].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k) > toSort[j].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k)){
							let tempSpot = toSort[i];
							toSort[i] = toSort[j];
							toSort[j] = tempSpot;
							break;
						}
						
						if (toSort[i].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k) < toSort[j].projectId.toString().replace(/ /g, "").toUpperCase().charAt(k)){
							break;
						}
						
						// ... important if the current String is "hello" and it's compared to "hello5". In this case, the previous for loop goes to completion, and this takes over.
						if (same && k == limit - 1){
							if (toSort[i].projectId.toString().length > toSort[j].projectId.toString().length){
								let tempSpot = toSort[i];
								toSort[i] = toSort[j];
								toSort[j] = tempSpot;
							}
						}
					}
				}
			}
		}
		
		return toSort;
	}

	//resets input fields, disables the add button, execute scratch(), updates status bar
	//used for reset button and for addProject function
	function resetInputs(){
		for (let i = 0; i < valSet.length; i++){
			valSet[i].value = "";
			checklist[i] = false;
			scratch();
		}
		addButton.setAttribute("disabled", "");

		//updating the status bar for a few seconds and returning it to default status message
		if (templateProjArray.length > 0){ setTimeout(() => {statusBar.firstElementChild.textContent = "All fields cleared. Fill all fields to validate the form in order to add the project.";}, 4000); // Arrow function
		}
		else{
			statusBar.firstElementChild.textContent = "All fields cleared. Fill all fields to validate the form in order to add the project.";
		}
	}

	//adds validated project to array and table and resets the input fields
	let wholeForm = document.getElementById("wholeForm");
	wholeForm.addEventListener("submit", function(e){
		e.preventDefault();
		for (let i = 0; i < checklist.length; i++){
			if (!checklist[i]){
				return;
			}
		}
		if (e.type === "submit"){
			let currentProj = {projectId: projectId.value, ownerName: ownerName.value, title: title.value, category: category.value, hours: hours.value, rate: rate.value, stats: stats.value, shortDesc: shortDesc.value};
			templateProjArray.push(currentProj);
			
			statusBar.firstElementChild.textContent = "Added " + projectId.value + " to table. There is/are now " + templateProjArray.length + " project(s) in the table and current project array.";
			
			buildTable(templateProjArray);
			resetInputs();
		}
	}, false);

	//not non, used more than once
	function saveLocal(){
		if (templateProjArray.length > 0){
			localStorage.setItem("projectSet", JSON.stringify(templateProjArray));
			statusBar.firstElementChild.textContent = "Table projects saved to localStorage (overwritten, or added if localStorage was empty). There is/are now " + JSON.parse(localStorage.getItem("projectSet")).length + " project(s) in localStorage.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 5000);
		}
		
		else{
			statusBar.firstElementChild.textContent = "Current project array empty. Nothing to save/overwrite.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
		}
	}

	 //clearing local storage on click event of clearLocal button
	let clearLocal = document.getElementById("clearLocal");
	clearLocal.addEventListener("click", function(){
		if(confirm("Are you sure you want to clear localStorage?")){
			localStorage.clear();

			//updating status bar and resetting it back to default after a few seconds
			statusBar.firstElementChild.textContent = "Cleared localStorage";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
		}
	}, false);

	//implements the readLocal button
	let readLocal = document.getElementById("readLocal");
	readLocal.addEventListener("click", function(){
		if (localStorage.length > 0){
			//execute if localStorage isnt empty
			let localRecord = JSON.parse(localStorage.getItem("projectSet"));
			combineArrays(localRecord, templateProjArray);
			buildTable(templateProjArray);
		}
		else{
			//execute if localStorage is empty
			statusBar.firstElementChild.textContent = "localStorage empty. Nothing to load.";
			setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
		}
	}, false);

	let querySearch = document.getElementById("querySearch");
	querySearch.addEventListener("keyup", function(){
		// If the search box is blank, it just loads the full table.
		if (querySearch.value === ""){ 
			buildTable(templateProjArray);
			statusBar.firstElementChild.textContent = "";
		}
		else{
			if (templateProjArray.length > 0){
				let searchResult = templateProjArray.filter(findArrays);
		
				function findArrays(eachProject){
					let searchTerm = querySearch.value;
					let found = false;
					
					/* Kind of a neat shortcut to typing the check for each field of the project object (8 manual checks).
					   Checks for a match in each field. If it does, rather than return right away, it sets a value, leaves 
					   the loop and then returns on that updated value. Otherwise, it would return in the first
					   iteration of the loop, whether true or false.*/
					for (let x in eachProject){
						if (eachProject[x].indexOf(searchTerm) > -1){
							found = true;
							break;
						}
					}
					
					return found;
				}
				
				buildTable(searchResult);
				statusBar.firstElementChild.textContent = "There is/are " + searchResult.length + " project(s) matching your query. (search is case sensitive)";
				setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 4000);
			}
			
			else{
				statusBar.firstElementChild.textContent = "Current project array and table empty. Nothing to search.";
				setTimeout(() => {statusBar.firstElementChild.textContent = "Fill all fields to validate the form in order to add the project.";}, 3000);
			}
		}
	}, false);

	let resetButton = document.getElementById("resetButton");
	resetButton.addEventListener("click", resetInputs, false);

	let writeLocal = document.getElementById("writeLocal");
	writeLocal.addEventListener("click", saveLocal, false);

}, false);

/*
  CREDITS:
  
  -toUpperCase (and toLowerCase) from Java I
  
  -Removing spaces to compare values: https://www.geeksforgeeks.org/how-to-remove-spaces-from-a-string-using-javascript/
  
  -setTimeout (used in previous labs): https://developer.mozilla.org/en-US/docs/Web/API/setTimeout
*/
